/**
 * Created by harsh on 12/7/17.
 */
import React from 'react';
import Box from './Box';
import * as Styled from '../../assets/css/theme'
class Board extends React.Component {
  
    getRows = (num) => {
        let {randomNumber, BoxClicked, disabled} = this.props;
        let htmlRows = [], htmlCols = [],number = num,count=0;
        for (var i = 1; i <= number; i++) {
            htmlCols = []
            for (var j = 1; j <= number; j++) {
                count=count+1
                htmlCols.push(<Box disabled={disabled} key={count} boxNumber={count} randomNumber={randomNumber} BoxClicked={BoxClicked} />); //Board column
            }
            htmlRows.push(<Styled.BoardRow key={i}>{htmlCols}</Styled.BoardRow>); //Board Row
        }
        return (htmlRows);

    }

    render() {
        const htmlRows = this.getRows(this.props.num)
        return (
            <Styled.Board    > {htmlRows} </Styled.Board>
        )
    }
}

export default Board;
