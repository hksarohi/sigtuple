/**
 * Created by harsh on 12/7/17.
 */
import React from 'react';

class ActionControls extends React.Component {

    constructor(props) {
        super(props)

    }

    start = () =>{
       this.props.start()
        
      }

     stop = () =>{
        this.props.stop()
      }

      restart = () =>{
        this.props.restart()
      }

    render() {
        return (
            <div className="actionControl">
                    <button className="start" onClick={this.start} disabled={this.props.disabled}>Start</button>
                    <button className="stop" onClick={this.stop} disabled={!this.props.disabled}>Stop</button>
                    <button className="restart" onClick={this.restart}>Restart</button>
            </div>
        )
    }
}

export default ActionControls;
