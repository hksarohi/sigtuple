/**
 * Created by harsh on 12/7/17.
 */
import React,{Fragment} from 'react';
import Board from './Board';
import ActionControls from './ActionControls';
import BoardSelection from './BoardSelection';
import CommonUtils from '../../utils/CommonUtils';
class Root extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            squarNum: 3,
            randomNumber:1,
            isDisable:false,
            disabled:true
        }
        this.timer=null
    }

    componentDidMount(){     
        this.timer = setInterval(this.intervalFun, 500);
    }

    componentWillUnmount(){
        clearInterval(this.timer);
        this.props.scoreReset()

    }

    start = () =>{
        clearInterval(this.timer);
        this.timer = setInterval(this.intervalFun, 500);
        this.setState({
            disabled:true
        })
    }

     stop = () =>{

         clearInterval(this.timer);
         this.setState({
            disabled:false
        })

      }

    restart = () => {
        clearInterval(this.timer);
        this.props.scoreReset()

        this.timer = setInterval(this.intervalFun, 500);
        this.setState({
            disabled:true
        })
    }

    intervalFun = () =>{
        const maximum = (this.state.squarNum*this.state.squarNum);
        const randomNumber = CommonUtils.randomNumber(1,maximum)
        this.setState({
            randomNumber:randomNumber
        })
    }

        
   
    changehandler = (e) => {
        this.props.scoreReset()

        this.setState({
            squarNum: e.target.value
        })
    }

    

    render() {

        const { squarNum,randomNumber,isDisable,disabled } = this.state;
        const { score } = this.props;
        const hscore = CommonUtils.getHscore();
        return (
           <Fragment>
                <h2>Dash Board</h2>

                <BoardSelection changehandler={this.changehandler}/>

                <h3 > <span> Score: {score}  </span>  <span>H-Score: {hscore}</span> </h3>
                  
                <Board disabled={!disabled} randomNumber={randomNumber}  num = {squarNum} BoxClicked={this.props.BoxClicked} />

                 <ActionControls start = {this.start} stop = {this.stop} restart = {this.restart} disabled={disabled}/>
              
           </Fragment>
        )
    }
}


export default Root;
