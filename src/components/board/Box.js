/**
 * Created by harsh on 12/7/17.
 */
import React from 'react';
import * as Styled from '../../assets/css/theme'

class Box extends React.Component {

boxClick = () =>{
    let {randomNumber,boxNumber} = this.props
    this.props.BoxClicked(randomNumber,boxNumber)
}
    
       render() {
        const {randomNumber, boxNumber, isDisable} = this.props;
        let className= randomNumber==boxNumber ? true:false;
        let Disable = (isDisable && (randomNumber==boxNumber)) ?  true : false;
        return (
            <Styled.Box onClick={this.boxClick}  disabled={Disable} className={className}></Styled.Box>
        )
    }
}

export default Box;
