/**
 * Created by harshsarohi on 10/10/18.
 */
import keyMirror from 'keymirror';

export const actions = keyMirror({
    INCREMENT: null,
    SCORE_RESET:null
});

