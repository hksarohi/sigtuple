class CommonUtils {


  static setHscore(score) {
      let hscore = localStorage.getItem('hscore');  
      if (score >= hscore) { 
        localStorage.setItem('hscore', score)
        return
       } 
    
  }

  static getHscore() {
    return localStorage.getItem('hscore');
  }

    static clone(oldObject, newObject) {
        return { ...oldObject, ...newObject };
    }

    static randomNumber = (minimum, maximum) => {
        return Math.round(Math.random() * (maximum - minimum) + minimum);
    }

}

module.exports = CommonUtils;
