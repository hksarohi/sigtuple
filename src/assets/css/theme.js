/**
 * Created by harshsarohi on 31/10/18.
 */
import styled from 'styled-components'

export const Board = styled.div`
    margin: 40px 0;
  
      }
`;

export const BoardRow=styled.div`

        display: flex;

        
       
`;
export const Box=styled.div`

         width: 64px;
         height: 64px;
          background: #d3d3d3;
         border: 1px solid #ffffff;
          cursor: pointer;
          &.active{
            background: #008000;
           }
               background: ${props => props.className ? "#008000" : "#d3d3d3"};
        
                ${props => props.disabled ? "opacity: 0.6;cursor: not-allowed;pointer-events: none;" : ""};

    
       
`;
export const ScoreTitle = styled.h3`
     
`;

