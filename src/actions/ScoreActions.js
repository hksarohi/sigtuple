/**
 * Created by harshsarohi on 10/10/18.
 */
import { actions } from '../constants/Constants';

export function increment() {
    return {
        type: actions.INCREMENT_DEBOUNCE,
    };
}

export function reset() {
    return {
        type: actions.SCORE_RESET
    };
}


/*export function scoreIncrement() {
    return (dispatch) => {
        dispatch(increment());
    };
}


export function scoreReset() {
    return (dispatch) => {
      dispatch(reset());
    };
  }*/