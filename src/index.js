import React from 'react';
import ReactDOM from 'react-dom';
import { createEpicMiddleware } from 'redux-observable';
import { createLogger } from 'redux-logger';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import createBrowserHistory from 'history/createBrowserHistory';
import BoardPage from './containers/BoardPage';
import { Router, Route, browserHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer,routerMiddleware } from 'react-router-redux'
const title = 'My Minimal React Webpack  Setup';
import { rootEpic } from './epics';
/**
 | Reducers
 */
import allReducers from './reducers';

/**
 | Constants
 */
const isDevelopmentMode = (process.env.NODE_ENV !== 'production');

/**
 | Stylesheets
 */


/**
 | Logger
 */
const logger = createLogger();

/**
 | History
 */
const history = createBrowserHistory();

/**
 | Middleware
 */
const middleware = routerMiddleware(history);


const epicMiddleware = createEpicMiddleware();


/**
 | Store
 */
let store = null;
if (isDevelopmentMode) {
    store = createStore(
        allReducers,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),

        applyMiddleware(middleware,  epicMiddleware,logger),
    );
} else {
    store = createStore(
        allReducers,
        applyMiddleware(middleware, epicMiddleware),
    );
}
epicMiddleware.run(rootEpic);

ReactDOM.render(
    <Provider store={store}>
        { /* Tell the Router to use our enhanced history */ }
        <Router history={history}>
            <div>

            <Route path="/test" component={BoardPage}>

            </Route>
            <Route path="/" component={BoardPage}>

            </Route>
            </div>
        </Router>
    </Provider>,
    document.getElementById('app')
);

module.hot.accept();