/**
 * Created by harsh on 12/7/17.
 */
import React from 'react';
import { connect } from 'react-redux';
import CommonUtils from '../utils/CommonUtils';
import { increment, reset } from '../actions/ScoreActions';
import Root from '../components/board/Root'

class BoardPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

        }

    }

    BoxClicked = (randomNumber, boxNumber) => {
        if (randomNumber == boxNumber) {
            this.props.increment();
        }


    }


    componentWillReceiveProps(nextProps) {
        const { score } = nextProps;
        CommonUtils.setHscore(score)
    }




    render() {
        const { score } = this.props;
        return (
            <Root scoreReset={this.props.reset} score={score} BoxClicked={this.BoxClicked} />
        )
    }
}



function mapStateToProps(state) {
    const { counts } = state;
    return {
        score: counts.score,
    };
}

export default connect(mapStateToProps, {
    increment, reset
})(BoardPage);
