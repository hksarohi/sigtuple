/**
 * Created by harshsarohi on 10/10/18.
 */
import { combineReducers } from 'redux';
import ScoreReducer from './ScoreReducer';


const allReducers = combineReducers({
    counts: ScoreReducer,

});

export default allReducers;
