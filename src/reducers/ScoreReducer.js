/**
 * Created by harshsarohi on 10/10/18.
 */
import { actions } from '../constants/Constants';
import CommonUtils from '../utils/CommonUtils';

const initialState = {
    score: 0
};

export default (state = initialState, action = {}) => {
    let changes = {};

    switch (action.type) {

        case actions.INCREMENT:
            changes = { score: state.score + 1 };
            break;

        case actions.SCORE_RESET:
            changes = { ...initialState };
            break;


        default:
            changes = {};
            break;
    }

    return CommonUtils.clone(state, changes);
};
