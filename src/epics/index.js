/**
 * Created by harshsarohi on 29/10/18.
 */
import { combineEpics } from 'redux-observable';


import { ofType } from 'redux-observable';

import { filter, mapTo,delay} from 'rxjs/operators';
import { debounceTime,throttleTime } from 'rxjs/operators';



const resetScore = action$ => action$.pipe(filter(action => action.type === 'RESET'), mapTo({ type: 'RESET' }));



const incrementScore = action$ => action$.pipe(
    ofType('INCREMENT_DEBOUNCE'),
    throttleTime(100),
    mapTo({ type: 'INCREMENT' })
);

// later...
export const rootEpic = combineEpics(incrementScore,resetScore);
